#!/usr/bin/python

import sys
import csv
import datetime
import requests
from bs4 import BeautifulSoup

if len(sys.argv) != 3:
    print("Please input 2 files: with websites and with output.")
    exit()
    
def check_files_input():
    """Checks wether the file exists.
    """
    try:
        open(sys.argv[1])
    except:
        print("File"+sys.argv[1]+"does not exist.")
        exit()

def check_files_output():
    """Checks wether the file exists, if does append date to the name.
    """
    try:
        open(sys.argv[2])
        sys.argv[2] = sys.argv[2][:-4]+'_'+datetime.date.today().strftime('%Y-%m-%d')+sys.argv[2][-4:]
        print("File already exist, creating new file "+sys.argv[2])
    except:
        pass

def counter_buttons(address):
    """Input: address.
    Checks buttons on the page with address.
    """
    address = address.rstrip()
    index_url = 'https://'+address+'/'
    r = requests.get(index_url)
    soup = BeautifulSoup(r.content,"html.parser")
    buttons_tag = soup.findAll('button')
    buttons_class = 0
    a = soup.findAll('a')
    for link in a:
        cl = link.get('class')
        if cl is not None:
            cl = ''.join(cl)
            if ('button' or 'btn') in cl:
                buttons_class = buttons_class+1
    buttons_type = 0
    inp = soup.findAll('input')
    for link in inp:
        ty = link.get('type')
        if ty is not None:
            if ('submit' or 'reset' or 'button') in ty:
                buttons_type = buttons_type+1
    return len(buttons_tag)+buttons_class+buttons_type

if __name__ == "__main__":
    check_files_input()
    check_files_output()
    sites = open(sys.argv[1],'r')
    unsort_addresses = []
    unsort_buttons = []
    for address in sites:
        address = address.rstrip()
        unsort_addresses.append(address)
        unsort_buttons.append(counter_buttons(address))
    sort_addresses = [addresses for __,addresses in sorted(zip(unsort_buttons,unsort_addresses),reverse=True)]
    sort_buttons = sorted(unsort_buttons,reverse=True)

    with open(sys.argv[2],'w') as csvfile:
        names = ['address','number_of_buttons']
        writer = csv.DictWriter(csvfile,fieldnames=names)
        writer.writeheader()
        for i in range(len(sort_addresses)):
            writer.writerow({'address':sort_addresses[i],'number_of_buttons':sort_buttons[i]})
            

