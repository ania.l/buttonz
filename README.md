# Buttonz 

This is a script to count buttons on a website.

### Prerequisites

The required libraries are: sys, csv, datetime, requests and bs4.

### Installing
To run the program you need two filenames as arguments: list of websites to be checked and a name for an output file. Example:
```
buttonz-counter.py files_with_websites counted_websites.csv
```
 
